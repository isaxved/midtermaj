create table if not exists person (
    id serial primary key,
    firstname varchar default 255,
    lastname varchar default 255,
    city varchar default 255,
    phone varchar default 255,
    telegram varchar default 255
);

alter table person owner to postgres;